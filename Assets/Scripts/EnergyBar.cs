﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The 'paddle' in this adaptation of Breakout
/// </summary>
public class EnergyBar : MonoBehaviour
{
	//public Transform centreBar;
	//public Transform leftEnd;
	//public Transform rightEnd;

	public float defaultWidth = 2;

	public float moveSpeed;
	public float moveRowTime = 0.75f;		// time to tween between rows
		
	public AudioClip whistleAudio;

	public KeyCode leftKey = KeyCode.LeftArrow;    			// between rows
	public KeyCode rightKey = KeyCode.RightArrow;    		// between rows
	public KeyCode paddleAwayKey = KeyCode.UpArrow;    		// move paddle away
	public KeyCode paddleCloserKey = KeyCode.DownArrow;    	// move paddle closer

	private List<TowerRow> towerRows = null; 			// set when city has been built
	private int belowRowIndex = 0;
	private bool canMovePaddle = false;
	private bool tweeningPaddleRow = false;
	
	private Vector3 startPosition;


	public TowerRow RowBelow { get { return towerRows != null ? towerRows[ belowRowIndex ] : null; } }


	private void Start()
	{
		startPosition = transform.position;
	}

	private void OnEnable()
	{
		BreakoutEvents.OnBuildCity += OnBuildCity;
		BreakoutEvents.OnCityBuilt += OnCityBuilt;
		BreakoutEvents.OnRestartGame += OnRestartGame;
			
		BreakoutEvents.OnChargesExpired += OnChargesExpired;
		BreakoutEvents.OnCityCleared += OnCityCleared;
	}

	private void OnDisable()
	{
		BreakoutEvents.OnBuildCity += OnBuildCity;
		BreakoutEvents.OnCityBuilt -= OnCityBuilt;
		BreakoutEvents.OnRestartGame -= OnRestartGame;
		
		BreakoutEvents.OnChargesExpired -= OnChargesExpired;
		BreakoutEvents.OnCityCleared -= OnCityCleared;
	}


	// update used to get input
	private void Update()
	{
		if (!canMovePaddle)
			return;
			
		//if (Input.GetAxis("Horizontal") < 0)
		if (Input.GetKey(leftKey))
			MoveLeft();

		//if (Input.GetAxis("Horizontal") > 0)
		if (Input.GetKey(rightKey))
			MoveRight();

		if (! tweeningPaddleRow)
		{
			if (Input.GetKeyDown(paddleAwayKey))
				MoveAway();

			if (Input.GetKeyDown(paddleCloserKey))
				MoveCloser();
		}
	}

	public void ResetPosition()
	{
		transform.position = startPosition;
	}

	private void SetBarWidth(float width)
	{
		transform.localScale = new Vector3(width, transform.localScale.y, transform.localScale.z);
		//leftEnd.localPosition = new Vector3(-width / 2f, 0, 0);
		//rightEnd.localPosition = new Vector3(width / 2f, 0, 0);
	}

	
	private void OnRestartGame()
	{
		canMovePaddle = false;          // until city rebuilt
	}
	
	private void OnChargesExpired()
	{
		canMovePaddle = false;  
	}
	
	private void OnCityCleared()
	{
		canMovePaddle = false;  
	}


	/// <summary>
	/// tweens paddle to under given rowIndex,
	/// sets its material accordingly
	/// </summary>
	/// <param name="towerRowIndex">Tower row index.</param>
	//private IEnumerator SetTowerRow(int towerRowIndex, bool silent)
	private void SetTowerRow(int towerRowIndex, bool silent)
	{
		belowRowIndex = towerRowIndex;

		//while (RowBelow.IsConsolidating)
			//yield return null;

		tweeningPaddleRow = true;
		
		if (!silent)
			AudioSource.PlayClipAtPoint(whistleAudio, transform.position);

		// tween energyBar on z axis
		LeanTween.moveZ(gameObject, RowBelow.transform.position.z, moveRowTime)
					.setEase(LeanTweenType.easeOutBounce)
					.setOnComplete(() =>
					{
						tweeningPaddleRow = false;
						SetMaterial(RowBelow.EnergyBarMaterial);
					});
		
		BreakoutEvents.OnEnergyBarSetTowerRow?.Invoke(this, RowBelow, belowRowIndex);
		//yield return null;
	}

	private void OnBuildCity(CityBuilder city)
	{
		canMovePaddle = false;
		
		SetBarWidth(defaultWidth);
		ResetPosition();
	}

	private void OnCityBuilt(CityBuilder city)
	{
		if (city.towerRows == null || city.towerRows.Count == 0)
			return;

		towerRows = city.towerRows;
			
		// set paddle to fixed distance from and underneath first row
		transform.position = new Vector3(transform.position.x, city.frontLeft.transform.position.y - city.cityToPaddleY, transform.position.z);

		// paddle starts under the first row
		//StartCoroutine(SetTowerRow(0, true));
		SetTowerRow(0, true);
		canMovePaddle = true;
	}

	private void SetMaterial(Material material)
	{
		foreach (var child in GetComponentsInChildren<Renderer>())
		{
			child.material = material;
		}
	}

	private void MoveCloser()
	{
		if (towerRows == null)
			return;

		if (tweeningPaddleRow)
			return;
			
		if (belowRowIndex > 0)
			belowRowIndex--;
		else
			belowRowIndex = towerRows.Count - 1; 		// loop round to back row
			
		//StartCoroutine(SetTowerRow(belowRowIndex, false));				// tweens to next row
		SetTowerRow(belowRowIndex, false);				// tweens to next row
		//BreakoutEvents.OnEnergyBarMoveCloser(this);
	}

	private void MoveAway()
	{
		if (towerRows == null)
			return;
			
		if (tweeningPaddleRow)
			return;

		if (belowRowIndex < towerRows.Count - 1)
			belowRowIndex++;
		else
			belowRowIndex = 0; 						// loop round to front row

		//StartCoroutine(SetTowerRow(belowRowIndex, false));			// tweens to next row
		SetTowerRow(belowRowIndex, false);			// tweens to next row
		//BreakoutEvents.OnEnergyBarMoveAway(this);
	}

	private void MoveRight()
	{
		if (towerRows == null || towerRows.Count == 0)
			return;

		if ((transform.position.x + (transform.localScale.y / 2f)) < RowBelow.paddleLimitRight.position.x)
		{
			var speed = moveSpeed * RowBelow.EnergyBarSpeedFactor;
			transform.position = Vector3.MoveTowards(transform.position, RowBelow.paddleLimitRight.position, speed * Time.deltaTime);
		}
	}

	private void MoveLeft()
	{
		if (towerRows == null || towerRows.Count == 0)
			return;

		if ((transform.position.x - (transform.localScale.y / 2f)) > RowBelow.paddleLimitLeft.position.x)
		{
			var speed = moveSpeed * RowBelow.EnergyBarSpeedFactor;
			transform.position = Vector3.MoveTowards(transform.position, RowBelow.paddleLimitLeft.position, speed * Time.deltaTime);
		}
	}
}

﻿using System.Collections.Generic;
using UnityEngine;

public static class BreakoutEvents
{
	public delegate void OnBuildCityDelegate(CityBuilder city);
	public static OnBuildCityDelegate OnBuildCity;
	
	public delegate void OnCityBuiltDelegate(CityBuilder city);
	public static OnCityBuiltDelegate OnCityBuilt;
	
	public delegate void OnTowerLevelBuiltDelegate();
	public static OnTowerLevelBuiltDelegate OnTowerLevelBuilt;
	
	public delegate void OnConsolidateCityDelegate();
	public static OnConsolidateCityDelegate OnConsolidateCity;
	
	
	public delegate void OnEnergyBarSetTowerRowDelegate(EnergyBar energyBar, TowerRow rowBelow, int rowIndex);
	public static OnEnergyBarSetTowerRowDelegate OnEnergyBarSetTowerRow;
		
	//public delegate void OnEnergyBarMoveAwayDelegate(EnergyBar energyBar);
	//public static OnEnergyBarMoveAwayDelegate OnEnergyBarMoveAway;
	
	//public delegate void OnEnergyBarMoveCloserDelegate(EnergyBar energyBar);
	//public static OnEnergyBarMoveCloserDelegate OnEnergyBarMoveCloser;
	
	public delegate void OnTowerLevelHitDelegate(PlasmaCharge charge);
	public static OnTowerLevelHitDelegate OnTowerLevelHit;
	
	public delegate void OnTowerLevelDemolishedDelegate(Tower parentTower);
	public static OnTowerLevelDemolishedDelegate OnTowerLevelDemolished;
	
	public delegate void OnTowerDemolishedDelegate();
	public static OnTowerDemolishedDelegate OnTowerDemolished;
	
	public delegate void OnBarChargeCollisionDelegate(Collision collision);
	public static OnBarChargeCollisionDelegate OnBarChargeCollision;
	
	public delegate void OnBarWallCollisionDelegate(Collision collision);
	public static OnBarWallCollisionDelegate OnBarWallCollision;
	
	public delegate void OnCatchChargeDelegate(PlasmaCharge charge);
	public static OnCatchChargeDelegate OnCatchCharge;
	
	public delegate void OnReleaseChargeDelegate(PlasmaCharge charge);
	public static OnReleaseChargeDelegate OnReleaseCharge;
	
	public delegate void OnChargeLostDelegate(PlasmaCharge charge);
	public static OnChargeLostDelegate OnChargeMissed;
	
		
	public delegate void OnUpdateScoreDelegate(int increment);
	public static OnUpdateScoreDelegate OnUpdateScore;
	
	public delegate void OnUpdateTimerDelegate();
	public static OnUpdateTimerDelegate OnUpdateTimer;
	
		
	public delegate void OnChargesExpiredDelegate();
	public static OnChargesExpiredDelegate OnChargesExpired;
	
	public delegate void OnCityClearedDelegate();
	public static OnCityClearedDelegate OnCityCleared;
	
	public delegate void OnTreesGrownDelegate();
	public static OnTreesGrownDelegate OnTreesGrown;
	
	
	// UI
	
	public delegate void OnScoreUpdatedDelegate(int newScore);
	public static OnScoreUpdatedDelegate OnScoreUpdated;
	
	public delegate void OnTimerUpdatedDelegate(int newSeconds);
	public static OnTimerUpdatedDelegate OnTimerUpdated;
	
	public delegate void OnChargesUpdatedDelegate(int newCharges);
	public static OnChargesUpdatedDelegate OnMissesUpdated;
	
	public delegate void OnTowerLevelsLeftUpdatedDelegate(int levelsLeft);
	public static OnTowerLevelsLeftUpdatedDelegate OnTowerLevelsLeftUpdated;
	
	public delegate void OnCurrentScoreDelegate(int currentScore);
	public static OnCurrentScoreDelegate OnCurrentScore;
	
	public delegate void OnFinalScoreDelegate(int finalScore);
	public static OnFinalScoreDelegate OnFinalScore;
	
	public delegate void OnRestartGameDelegate();
	public static OnRestartGameDelegate OnRestartGame;
}
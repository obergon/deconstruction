﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerRow : MonoBehaviour
{
	public Transform paddleLimitLeft;
	public Transform paddleLimitRight;

	private List<Tower> towers = new List<Tower>();
	
	public int RowScore { get; private set; }                   // score value of TowerLevels in this row
	public float PlasmaChargeSpeedFactor { get; private set; }  // amount ball speeds up under this row
	public float EnergyBarSpeedFactor { get; private set; }     // amount paddle movement speeds up under this row
	public Material EnergyBarMaterial { get; private set; }     // material assigned to paddle when under this row

	public bool IsConsolidating { get; private set; }  

	public void AddTower(Tower tower)
	{
		towers.Add(tower);
	}

	//public void RemoveTower(Tower tower)
	//{
	//	towers.Remove(tower);
	//}

	public void SetRowData(int row, int score, float chargeSpeed, float barSpeed, Material rowMaterial)
	{
		//rowNumber = row;
		RowScore = score;
		PlasmaChargeSpeedFactor = chargeSpeed;
		EnergyBarSpeedFactor = barSpeed;
		EnergyBarMaterial = rowMaterial;
	}

	public void ConsolidateRow()
	{
		IsConsolidating = true;
		
		foreach (var tower in towers)
		{
			tower.ConsolidateAllLevels();
		}

		IsConsolidating = false;
	}

	public int TowerLevelsLeft
	{
		get
		{
			int levelsLeft = 0;
			
			foreach (var tower in towers)
			{
				levelsLeft += tower.LevelsLeft;
			}

			return levelsLeft;
		}
	}
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
	public float shrinkTime;
	
	public float dropDelay;
	public float dropTime;
	
	public AudioClip demolishAudio;
	public AudioClip dropAudio;

	private TowerRow parentRow;
	private List<TowerLevel> towerLevels = new List<TowerLevel>();
	private bool towerDropping = false;

	private bool hasDemolition = false;			// 'dirty' flag for consolidation

	public int LevelsLeft
	{
		get
		{
			//return towerLevels.Count;
			int count = 0;
			
			foreach (var towerLevel in towerLevels)
			{
				if (towerLevel.isDemolished)
					continue;

				count++;
			}
			return count;
		}
	}
	

	public void AddLevel(TowerLevel level)
	{
		towerLevels.Add(level);
	}
	
	public void SetParentRow(TowerRow parent)
	{
		parentRow = parent;
	}

	public void DemolishLevel(TowerLevel towerLevel)
	{
		AudioSource.PlayClipAtPoint(demolishAudio, transform.position);

		//if (towerLevel.isDropping)
			//return;
			
		towerLevel.DisableColliders();
		towerLevel.DemolishParticles();
		
		// demolition score looked up from parentRow
		BreakoutEvents.OnUpdateScore?.Invoke(parentRow.RowScore);
					
		LeanTween.scaleY(towerLevel.gameObject, 0f, shrinkTime)
					.setEase(LeanTweenType.easeInQuint)
					.setOnComplete(() =>
					{
						//towerLevels.Remove(towerLevel);
						//ReIndexLevels();
						
						//Destroy(towerLevel.gameObject);
						towerLevel.DisableRenderers();
						towerLevel.VapouriseParticles();
								
						BreakoutEvents.OnTowerLevelDemolished?.Invoke(this);
						
						hasDemolition = true; 		// 'dirty' flag

						//DropLevelsAbove(levelIndex);
																
						//if (LevelsLeft == 0)
							//BreakoutEvents.OnTowerDemolished?.Invoke();
					});
	}
	
	public void ConsolidateAllLevels()
	{
		//if (towerDropping)
			//return;

		if (!hasDemolition)
			return;

		float levelPosition = 0;

		//towerDropping = true;

		foreach (var towerLevel in towerLevels)
		{
			if (towerLevel.isDemolished)
				continue;

			towerLevel.isDropping = true;
			levelPosition += towerLevel.LevelHeight;

			if (towerLevel.transform.localPosition.y == levelPosition)
				continue;

			LeanTween.moveLocalY(towerLevel.gameObject, levelPosition, dropTime)
						.setEase(LeanTweenType.easeInQuint)
						.setDelay(dropDelay)
						.setOnComplete(() =>
						{
							AudioSource.PlayClipAtPoint(dropAudio, transform.position, 0.25f);
							towerLevel.DustParticles();
							towerLevel.isDropping = false;
						});
		}
		
		//towerDropping = false;
		hasDemolition = false;
	}

	//private void DropLevelsAbove(int levelIndex)
	//{
	//	if (towerDropping)
	//		return;

	//	towerDropping = true;
			
	//	for (int i = levelIndex; i < towerLevels.Count; i++)
	//	{
	//		TowerLevel towerLevel = towerLevels [ i ];
	//		towerLevel.isDropping = true;

	//		LeanTween.moveY(towerLevel.gameObject, towerLevel.transform.position.y -0.5f, dropTime)
	//			.setEase(LeanTweenType.easeInOutQuint)
	//			.setDelay(dropDelay)
	//			.setOnComplete(() =>
	//			{
	//				AudioSource.PlayClipAtPoint(dropAudio, transform.position);
	//				towerLevel.isDropping = false;
	//			});
	//	}
		
	//	towerDropping = false;
	//}
	
	//private void ReIndexLevels()
	//{
	//	for (int i = 0; i < towerLevels.Count; i++)
	//	{
	//		TowerLevel towerLevel = towerLevels [ i ];
	//		towerLevel.levelIndex = i;
	//	}
	//}
}

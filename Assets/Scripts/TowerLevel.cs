﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerLevel : MonoBehaviour
{
	public Tower ParentTower { get; private set; }
	[HideInInspector]
	public int levelIndex;
	
    private List<TowerBrick> levelBricks  = new List<TowerBrick>();
	public bool isDropping;
	
	public float LevelHeight { get { return (levelBricks.Count > 0) ? levelBricks[0].transform.localScale.y : 0f; } }

	public bool isDemolished { get; private set; }		// to prevent level demolition more than once, caused by >1 brick


	public void SetParentTower(Tower parentTower, int levelIndex)
	{
		this.ParentTower = parentTower;
		this.levelIndex = levelIndex;
	}

	internal void AddLevelBrick(TowerBrick newBrick)
	{
		levelBricks.Add(newBrick);
	}

	public void Demolish()
	{
		if (isDemolished)
			return;
			
		// demolition done by Tower so it can re-index / drop other towers
		ParentTower.DemolishLevel(this);  //levelIndex);

		isDemolished = true;
	}

	public void DisableColliders()
	{
		foreach (var brick in levelBricks)
		{
			brick.GetComponent<Collider>().enabled = false;
		}
	}
	
	public void DisableRenderers()
	{
		foreach (var brick in levelBricks)
		{
			brick.GetComponent<Renderer>().enabled = false;
		}
	}
	
	public void DemolishParticles()
	{
		foreach (var brick in levelBricks)
		{
			brick.PlayDemolishParticles();
		}
	}
	
	public void DustParticles()
	{
		foreach (var brick in levelBricks)
		{
			brick.PlayDustParticles();
		}
	}
	
	public void VapouriseParticles()
	{
		foreach (var brick in levelBricks)
		{
			brick.VapouriseParticles();
		}
	}
}

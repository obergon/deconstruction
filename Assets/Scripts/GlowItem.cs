﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GlowItem : MonoBehaviour
{
	public List<Renderer> glowRenderers;		// set in Inspector
	
	private bool isGlowing = false;
		
		
	public void Glow(Material glowMaterial, int glowLayers)
	{
		if (isGlowing && glowMaterial != null)
			return;
			
		if (!isGlowing && glowMaterial == null)
			return;
	
		foreach (var glowRenderer in glowRenderers)
		{ 
			if (glowRenderer.materials.Length >= 2)
			{
				//Debug.Log("Glow: " + glowMaterial);
				
				var matArray = glowRenderer.materials;
				matArray[0] = glowRenderer.sharedMaterial;
				matArray[1] = glowMaterial;
				glowRenderer.materials = matArray;
	
				isGlowing = glowMaterial != null;
			}
		}
	}
}

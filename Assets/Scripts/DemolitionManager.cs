﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemolitionManager : MonoBehaviour
{
	public AudioClip gameOverAudio;  	
	public AudioClip cityClearedAudio;

	// trees!
	public GameObject treePrefab;       // for when city cleared
	public Transform treeFolder;       // for when city cleared

	public Transform ground;
	public Color greyGroundColour;
	public Color greenGroundColour;
	
	public float treeMinScale;
	public float treeMaxScale;
	public float treeMinTime;
	public float treeMaxTime;
	public float treeMinDelay;
	public float treeMaxDelay;

	private float shrinkTimeFactor = 0.25f;		// trees shrink much faster than they grow!
	
	public float treeXRange;		// +/- random spawn position
	public float treeZRange;		// +/- random spawn position
	public float treeYPos;			// spawn position
	
	public int numSpawnTrees;
	private List<GameObject> treePool = new List<GameObject>();

	// score
	private int demolitionScore;
	private int timerSeconds;
	private int misses;
	
	private int currentScore;
	private int finalScore;
		
	private bool timerPaused = false;

	private IEnumerator timerCoroutine = null;


	private void Start()
	{
		LeanTween.init(1600);
	}

	private void OnEnable()
	{
		BreakoutEvents.OnBuildCity += OnBuildCity;
		BreakoutEvents.OnCityBuilt += OnCityBuilt;
		BreakoutEvents.OnUpdateScore += OnUpdateScore;
		BreakoutEvents.OnUpdateTimer += OnUpdateTimer;
		BreakoutEvents.OnChargeMissed += OnChargeLost;
		
		BreakoutEvents.OnChargesExpired += OnChargesExpired;
		BreakoutEvents.OnCityCleared += OnCityCleared;

		BuildTreePool();
	}
	
	private void OnDisable()
	{
		BreakoutEvents.OnBuildCity -= OnBuildCity;
		BreakoutEvents.OnCityBuilt -= OnCityBuilt;
		BreakoutEvents.OnUpdateScore -= OnUpdateScore;
		BreakoutEvents.OnUpdateTimer -= OnUpdateTimer;
		BreakoutEvents.OnChargeMissed -= OnChargeLost;
		
		BreakoutEvents.OnChargesExpired -= OnChargesExpired;
		BreakoutEvents.OnCityCleared -= OnCityCleared;
		
		ClearTreePool();
	}
	

	private void BuildTreePool()
	{
		for (int i = 0; i < numSpawnTrees; i++)
		{
			var newTree = Instantiate(treePrefab, treeFolder.transform);

			float xPos = UnityEngine.Random.Range(-treeXRange, treeXRange);
			float zPos = UnityEngine.Random.Range(-treeZRange, treeZRange);
			float scale = UnityEngine.Random.Range(treeMinScale, treeMaxScale);
			float rotationY = UnityEngine.Random.Range(0, 359);

			newTree.transform.localScale = Vector3.zero;
			newTree.transform.position = new Vector3(xPos, treeYPos, zPos);
			newTree.transform.Rotate(new Vector3(0, rotationY, 0));

			treePool.Add(newTree);
		}
	}

	private void GrowTrees()
	{
		foreach (var tree in treePool)
		{
			float growScale = UnityEngine.Random.Range(treeMinScale, treeMaxScale);
			float growTime = UnityEngine.Random.Range(treeMinTime, treeMaxTime);
			float growDelay = UnityEngine.Random.Range(treeMinDelay, treeMaxDelay);

			LeanTween.scale(tree, new Vector3(growScale, growScale, growScale), growTime)
										.setDelay(growDelay)
										.setEase(LeanTweenType.easeInOutBounce);
		}
		
		LeanTween.color(ground.gameObject, greenGroundColour, treeMaxTime)
			.setOnComplete(() =>
			{
				BreakoutEvents.OnTreesGrown?.Invoke();
			});
	}
	
	private void ShrinkTrees()
	{
		LeanTween.color(ground.gameObject, greyGroundColour, treeMaxTime);
		
		foreach (var tree in treePool)
		{
			float shrinkTime = UnityEngine.Random.Range(treeMinTime, treeMaxTime) * shrinkTimeFactor;
			float shrinkDelay = UnityEngine.Random.Range(treeMinDelay, treeMaxDelay) * shrinkTimeFactor;

			LeanTween.scale(tree, Vector3.zero, shrinkTime)
										.setDelay(shrinkDelay)
										.setEase(LeanTweenType.easeInCirc);
		}
	}

	private void ClearTreePool()
	{
		foreach (var tree in treePool)
		{
			Destroy(tree);
		}
	}


	private void StopTimer()
	{
		if (timerCoroutine != null)
		{
			StopCoroutine(timerCoroutine);
			timerCoroutine = null;
		}
		
		timerPaused = true;
	}
	
	private void ResetTimer()
	{
		StopTimer();
		timerPaused = false;
		
		timerSeconds = 0;
		BreakoutEvents.OnTimerUpdated?.Invoke(timerSeconds);
	}
	
	private void StartTimer()
	{
		StopTimer();
		ResetTimer();
				
		timerPaused = false;

		timerCoroutine = StartTimeCount();
		StartCoroutine(timerCoroutine);
	}

	private IEnumerator StartTimeCount()
	{
		while (!timerPaused)
		{
			yield return new WaitForSeconds(1);
			timerSeconds++;
			BreakoutEvents.OnTimerUpdated?.Invoke(timerSeconds);
		}

		yield return null;
	}

	private void OnBuildCity(CityBuilder city)
	{
		ShrinkTrees();			// + tween ground back to grey
		ResetScore();
	}
	
	private void OnCityBuilt(CityBuilder city)
	{
		StartTimer();
	}

	private void OnChargeLost(PlasmaCharge charge)
	{
		AddMiss();
		
		if (misses <= 0)
			BreakoutEvents.OnChargesExpired?.Invoke(); 			// game over
	}
	
	private void AddMiss()
	{
		misses++;
		BreakoutEvents.OnMissesUpdated?.Invoke(misses);
		
		CalculateCurrentScore(false);
	}
	
	private void OnChargesExpired()
	{
		StopTimer();
		AudioSource.PlayClipAtPoint(gameOverAudio, Vector3.zero);
	}
	
	private void OnCityCleared()
	{
		StopTimer();
		GrowTrees();		// + tween ground to green
		
		AudioSource.PlayClipAtPoint(cityClearedAudio, Vector3.zero);

		CalculateCurrentScore(true);
	}

	private void CalculateCurrentScore(bool final)
	{
		//finalScore = demolitionScore / (timerSeconds > 0 ? timerSeconds : 1) / (misses > 0 ? misses : 1);	
		
		int newScore = (demolitionScore - timerSeconds) / (misses > 0 ? misses : 1);

		currentScore = newScore;
		BreakoutEvents.OnCurrentScore?.Invoke(currentScore);
			
		if (final)
		{
			finalScore = newScore;
			BreakoutEvents.OnFinalScore?.Invoke(finalScore);
		}
	}

	private void OnUpdateTimer()
	{
		timerSeconds++;
		BreakoutEvents.OnTimerUpdated?.Invoke(timerSeconds);
		
		CalculateCurrentScore(false);
	}

	private void OnUpdateScore(int increment)
	{
		demolitionScore += increment;
		BreakoutEvents.OnScoreUpdated?.Invoke(demolitionScore);
		
		CalculateCurrentScore(false);
		
		// TODO: temp to test trees!
		//if (demolitionScore > 500)
			//BreakoutEvents.OnCityCleared?.Invoke();
	}

	
	private void ResetScore()
	{
		demolitionScore = 0;
		timerSeconds = 0;
		misses = 0;
		currentScore = 0;
		finalScore = 0;

		BreakoutEvents.OnMissesUpdated?.Invoke(misses);
		BreakoutEvents.OnTimerUpdated?.Invoke(timerSeconds);
		BreakoutEvents.OnScoreUpdated?.Invoke(demolitionScore);
		BreakoutEvents.OnCurrentScore?.Invoke(currentScore);

		ResetTimer();
	}
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerBrick : MonoBehaviour
{
	[HideInInspector]
	public TowerLevel parentLevel;
	
	public ParticleSystem demolishParticles;
	public ParticleSystem dustParticles;
	public ParticleSystem vapourParticles;

	public Material glowMaterial;
	private GlowItem glowItem;
	
	private int hitCount = 0;

	private void Start()
	{
		glowItem = GetComponent<GlowItem>();
	}

	public void DemolishTowerLevel()
	{
		parentLevel.Demolish();		
	}

	public void PlayDemolishParticles()
	{
		demolishParticles.Play();
	}
	
	public void PlayDustParticles()
	{
		dustParticles.Play();
	}
	
	public void VapouriseParticles()
	{
		vapourParticles.Play();
	}

	public void TakeHit()
	{
		hitCount++;
	}

	public void Glow(int glowLayers = 1)
	{
		if (glowItem = null)
			return;

		glowItem.Glow(glowMaterial, glowLayers);
	}
}

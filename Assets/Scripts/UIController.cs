﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
	public Button restartButton;
	
	public Text score;
	public Text timer;
	public Text misses;            	// missed paddle (hit bottom wall)
	public Text currentScore;       // calculated
	public Text finalScore;         // after clearing city

	public Text levelsLeft;         // to be demolished
	
	public Text gameName;         	// 'deconstruction'

	public Image gameNamePanel;
	public Image gameOverPanel;
	public Image cityClearedPanel;


	private void OnEnable()
	{
		restartButton.onClick.AddListener(RestartGame);
		
		BreakoutEvents.OnScoreUpdated += OnScoreUpdated;
		BreakoutEvents.OnTimerUpdated += OnTimerUpdated;
		BreakoutEvents.OnMissesUpdated += OnMissesUpdated;
		
		BreakoutEvents.OnBuildCity += OnBuildCity;
		BreakoutEvents.OnCityBuilt += OnCityBuilt;
		
		BreakoutEvents.OnTowerLevelsLeftUpdated += OnTowerLevelsUpdated;
		
		BreakoutEvents.OnChargesExpired += OnChargesExpired;
		BreakoutEvents.OnCityCleared += OnCityCleared;
		BreakoutEvents.OnTreesGrown += OnTreesGrown;
		
		BreakoutEvents.OnCurrentScore += OnCurrentScore;
		BreakoutEvents.OnFinalScore += OnFinalScore;
	}

	private void OnDisable()
	{
		restartButton.onClick.RemoveListener(RestartGame);
		
		BreakoutEvents.OnScoreUpdated -= OnScoreUpdated;
		BreakoutEvents.OnTimerUpdated -= OnTimerUpdated;
		BreakoutEvents.OnMissesUpdated -= OnMissesUpdated;
		
		BreakoutEvents.OnBuildCity -= OnBuildCity;
		BreakoutEvents.OnCityBuilt -= OnCityBuilt;
		
		BreakoutEvents.OnTowerLevelsLeftUpdated -= OnTowerLevelsUpdated;
		
		BreakoutEvents.OnChargesExpired -= OnChargesExpired;
		BreakoutEvents.OnCityCleared -= OnCityCleared;
		BreakoutEvents.OnTreesGrown -= OnTreesGrown;
		
		BreakoutEvents.OnCurrentScore -= OnCurrentScore;
		BreakoutEvents.OnFinalScore -= OnFinalScore;
	}

	private void OnScoreUpdated(int newScore)
	{
		score.text = "POINTS: " + newScore.ToString();
	}
	
	private void OnCurrentScore(int score)
	{
		currentScore.text = "SCORE: " + score.ToString();
	}
	
	private void OnFinalScore(int score)
	{
		finalScore.text = "FINAL SCORE: " + score.ToString();
	}
	
	private void OnTimerUpdated(int timeInSeconds)
	{
		int minutes = Math.Abs(timeInSeconds / 60);
		int remainder = Math.Abs(timeInSeconds % 60);
		string seconds = (remainder < 10) ? ("0" + remainder.ToString()) : remainder.ToString();

		timer.text = "TIME: " + (timeInSeconds < 0 ? "-" : "") + minutes + ":" + seconds;
	}
	
	private void OnMissesUpdated(int missCount)
	{
		misses.text = "MISSES: " + missCount.ToString();
	}
	
	private void OnTowerLevelsUpdated(int levels)
	{
		levelsLeft.text = "LEVELS: " + levels.ToString();
	}


	private void OnBuildCity(CityBuilder city)
	{
		restartButton.interactable = false;

		gameNamePanel.gameObject.SetActive(true);
		gameOverPanel.gameObject.SetActive(false);
		cityClearedPanel.gameObject.SetActive(false);
	}
	
	private void OnCityBuilt(CityBuilder city)
	{
		restartButton.interactable = true;
		gameNamePanel.gameObject.SetActive(false);

		//BreakoutEvents.OnTowerLevelsLeftUpdated?.Invoke(city.TowerLevelsLeft);
	}
	
	
	private void OnChargesExpired()
	{
		gameOverPanel.gameObject.SetActive(true);
	}
	
	private void OnCityCleared()
	{
		restartButton.interactable = false;
		cityClearedPanel.gameObject.SetActive(true);
	}
	
	private void OnTreesGrown()
	{
		restartButton.interactable = true;
	}


	private void RestartGame()
	{
		BreakoutEvents.OnRestartGame?.Invoke();
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundingWalls : MonoBehaviour
{
	public Transform TopWall;
	public Transform BottomWall;
	public Transform LeftWall;
	public Transform RightWall;

	public ParticleSystem MissParticles;
	public float missTime;			// duration for bottom wall renderer to show on miss
	
	public Light MainLight;
	public Color DefaultLightColour;
	public Color MissLightColour;
	
	//private Rect playArea;
	
	//private const float wallThickness = 2.0f;
	//private const float hudHeight = 0.9f;
	
	private void Awake()
	{
		MainLight.color = DefaultLightColour;
	}


	private void OnEnable()
	{
		BreakoutEvents.OnChargeMissed += OnMiss;
	}
	
	private void OnDisable()
	{
		BreakoutEvents.OnChargeMissed -= OnMiss;
	}

	private void OnMiss(PlasmaCharge charge)
	{
		MissParticles.transform.position = charge.transform.position;
		MissParticles.Play();
		
		StartCoroutine(ShowBottomWall());
	}
	
	private IEnumerator ShowBottomWall()
	{
		var bottomRenderer = BottomWall.GetComponent<Renderer>();
		bottomRenderer.enabled = true;
		MainLight.color = MissLightColour;
		yield return new WaitForSeconds(missTime);
		bottomRenderer.enabled = false;
		MainLight.color = DefaultLightColour;
		yield return null;
	}


	//void Start()
    //{
		//FitToCamera();
    //}


	// fill out walls to edge of screen (camera area)
	//private void FitToCamera()
	//{
		//// // UnityScript
		//var dist = (transform.position - Camera.main.transform.position).z;
		
		//var leftBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, dist)).x;
		//var rightBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, dist)).x;
		 
 	//	var topBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, dist)).y;
 	
 		//var topRight = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, Camera.main.nearClipPlane));
 		//var bottomLeft = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, Camera.main.nearClipPlane));
 	
		//var bottomBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, dist)).y;
		 
		//transform.position.x = Mathf.Clamp(transform.position.x, leftBorder, rightBorder);
		//transform.position.y = Mathf.Clamp(transform.position.y, topBorder, bottomBorder);
 
 
		//var cameraHeight = Camera.main.orthographicSize * 2.0f;
		//playArea = new Rect(0, 0, cameraHeight * Camera.main.aspect, cameraHeight);

		//var halfPlayWidth = playArea.width / 2.0f;
		//var halfPlayHeight = playArea.height / 2.0f;

		//var verticalWallWidth = wallThickness;  //(cameraRect.width - playArea.width) / 2.0f;
		//var horizontalWallHeight = wallThickness;  //(cameraRect.height - playArea.height) / 2.0f;


		//var topWallCentre = halfPlayHeight + (horizontalWallHeight / 2.0f);
		//var rightWallCentreX = halfPlayWidth + (verticalWallWidth / 2.0f);
		//var rightWallCentreY = -(hudHeight / 2.0f);

		//var horizontalWallWidth = playArea.width + (verticalWallWidth * 2);
		//var verticaWallHeight = playArea.height - hudHeight;

		//TopWall.localPosition = new Vector3(0, topWallCentre - hudHeight, 0);
		////TopWall.localScale = new Vector3(horizontalWallWidth, horizontalWallHeight, wallThickness);

		//BottomWall.localPosition = new Vector3(0, -topWallCentre, 0);
		////BottomWall.localScale = new Vector3(horizontalWallWidth, horizontalWallHeight, wallThickness);

		//LeftWall.localPosition = new Vector3(-rightWallCentreX, rightWallCentreY, 0);
		////LeftWall.localScale = new Vector3(verticalWallWidth, verticaWallHeight, wallThickness);

		//RightWall.localPosition = new Vector3(rightWallCentreX, rightWallCentreY, 0);
		//RightWall.localScale = new Vector3(verticalWallWidth, verticaWallHeight, wallThickness);
	//}
}
